---
title: 'Surgery Date Is July 29th'
subtitle: ''
date: '2022-07-18'
draft: false
tags: [bav,choa,surgery]
author: Steven Ricks
---

I'll undergo heart surgery at Children's Healthcare of Atlanta* on Friday, July 29th. I'll go to the hospital on Thursday the 28th for a full day of pre-op tasks.

*That's right, 34 years old and having surgery at the children's hospital...
