---
title: "One Week Away"
subtitle: ''
date: 2022-07-22T09:35:41-04:00
draft: false
tags: [surgery,recovery,pre-surgery]
author: Steven Ricks
---

It's one week until surgery. Lots of people ask me how I'm feeling about the surgery. I feel pretty neutral about it. I'm not excited about the first few days of recovery, but it will be nice to have the surgery done and behind me.

We've managed to knock out almost all of the "must do" items on our pre-surgery todo list. There's still a lot that would be nice to get done, and I still have a guitar project to finish this weekend, but I'm feeling better about the number of todo items.

Stay tuned for more updates as surgery gets even closer.