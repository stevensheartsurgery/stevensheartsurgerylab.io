---
title: Steven's Heart Surgery
comments: false
---

## Welcome

I've created this blog to post updates about my surgery and recovery and to make an account of my experience available to other people undergoing valve replacement surgery.

Check out the pages in the nav bar to find out more about my condition and valve replacement surgery, or read through the posts (below) for the latest updates.

## Get Updates Via Email

If you'd like to receive each new blog post via email, you can sign up for my newsletter by clicking [this link](https://buttondown.email/sricks3).