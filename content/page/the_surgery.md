---
title: "The Surgery"
subtitle: ''
draft: false
tags: []
---

## Overview

[Dr. Rosenblum](http://surgery.emory.edu/about-us/faculty_directory/faculty_profile_joshua_rosenblum.html) and [Dr. Chai](http://www.surgery.emory.edu/about-us/faculty_directory/faculty_profile_paul_chai.html), both of Emory University Hospital and Children's Hospital of Atlanta, will be opening up my chest on July 29th, 2022 to address my bicuspid aortic valve and dilated ascending aorta.

## Aortic Dilation

My ascending aorta is highly dilated. As I understand it, this is mostly due to my heart having to work harder because of the backflow (aortic regurgitation) through my bicuspid aortic valve. The surgeons will remodel my ascending aorta and aortic root to fix this dilation.

## Valve Option 1: Valve-sparing Aortic Root Replacement

This is the goal. The hope is that replacing my aortic root with one that is durable and has not become dilated will also mean that I can continue to live with my current aortic valve (even though it's bicuspid). My understanding is that once the valve is re-implanted in the new aortic root, it's regurgitation rate will be back at an acceptable level and could be expected to stay that way.

[Click here](https://my.clevelandclinic.org/health/treatments/17421-valve-sparing-or-valve-preserving-surgery-reimplantation-surgery) for more information on this procedure.

This would mean that I don't need a replacement valve, which (as explained below) would also mean that I don't need to be on blood thinners for the rest of my life.

Then downside is that the doctors will not know for sure if this is a viable option until they have me opened up. Dr. Rosenblum gave it a 50/50 chance during my consultation. That's why we have Option 2 as the backup.

## Valve Option 2: On-X Mechanical Aortic Valve

The backup plan is to replace my aortic valve with an [On-X prosthetic valve](https://www.cryolife.com/products/on-x-heart-valves/). Technically, a replacement tissue valve (using tissue from a cow or pig) is an option, but they only last 10-15 years, whereas a mechanical valve will likely last the rest of my life.

The upside is durability; the downside is that the mechanical valve would require me to take anticoagulant medication (specifically [warfarin](https://en.wikipedia.org/wiki/Warfarin)) for the rest of my life. This is mostly an inconvenience; needing to take the medicine constantly, monitor my blood, and regulate things like [vitamin K intake](https://www.hsph.harvard.edu/nutritionsource/vitamin-k/) would be more annoying than harmful or disruptive. I would have to be careful about internal bleeding though; head injuries, specifically, could be very serious.

Also, mechanical valves make a quiet-but-audible clicking sound when the heart beats, so I guess that would be a fun party trick and an easy way to keep track of my pulse.