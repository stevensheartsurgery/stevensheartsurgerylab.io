---
title: About BAV
subtitle: 
comments: false
---

## BAV (Bicuspid Aortic Valve)

I was born with a bicuspid aortic valve (BAV). Simply put, the aortic valve that controls blood flow out of your heart and into your aorta is supposed to have three leaflets, but in an estimated 1% of people it only had two leaflets. I'm in that 1%.

For more information, [here's the Wikipedia article](https://en.wikipedia.org/wiki/Bicuspid_aortic_valve).

## Why is it a problem?

There are a number of reasons, but it mostly comes down to two things which can cause other issues:

- BAVs tend to leak a little bit of blood backwards from the aorta into the left ventricle each time the heart beats (aortic regurgitation)
- BAVs also have a tendency to calcify over time which affects valve function and blood flow

In my case, it has also lead to a dilated ascending aorta. If this goes unfixed, it could result in aortic dissection.

## 1% is kind of a lot of people

Yeah, it is. Mine was diagnosed shortly after birth, but there's a huge range of severity. Lots of people don't even find out that they have a BAV until they're in their 50s or even older. Even for a lot of people who know about it, there might not be any outwardly perceivable symptoms. I've never had any symptoms that didn't have to be measured with an echo or an EKG, and yet mine's serious enough to need surgery now. After my first Facebook post about needing surgery, I found out that several people I know also have BAVs.

There's a 99% chance that you don't have one, but if you experience chest pain, shortness of breath, trouble breathing, fainting, or anything else out of the ordinary, it's worth a trip to a doctor. It's probably not BAV, but it might be something.